INSERT INTO franchise (fr_id, fr_name, fr_desc) VALUES (1, 'The Lord of the Rings', 'A fantasy novel series written by J. R. R. Tolkien');
INSERT INTO franchise (fr_id, fr_name, fr_desc) VALUES (2, 'Harry Potter', 'A novel series written by British author J.K. Rowling');
INSERT INTO franchise (fr_id, fr_name, fr_desc) VALUES (3, 'The Marvel Cinematic Universe', 'A media franchise and shared universe centered on a series of superhero films');

INSERT INTO movie (movie_id, movie_title, movie_genre, movie_year, movie_director, movie_picture, movie_trailer,franchise_id)
VALUES (1, 'The Fellowship of the Ring', 'Fantasy, Adventure', 2001, 'Peter Jackson', 'https://movies.com/fellowship-ring', 'https://youtube.com/fellowship-ring',1);
INSERT INTO movie (movie_id, movie_title, movie_genre, movie_year, movie_director, movie_picture, movie_trailer,franchise_id)
VALUES (2, 'The Two Towers', 'Fantasy, Adventure', 2002, 'Peter Jackson', 'https://movies.com/two-towers', 'https://youtube.com/two-towers',1);
INSERT INTO movie (movie_id, movie_title, movie_genre, movie_year, movie_director, movie_picture, movie_trailer,franchise_id)
VALUES (3, 'The Return of the King', 'Fantasy, Adventure', 2003, 'Peter Jackson', 'https://movies.com/return-king', 'https://youtube.com/return-king',1);
INSERT INTO movie (movie_id, movie_title, movie_genre, movie_year, movie_director, movie_picture, movie_trailer,franchise_id)
VALUES (4, 'Harry Potter and the Philosophers Stone', 'Fantasy, Adventure', 2001, 'Chris Columbus', 'https://movies.com/philosophers-stone', 'https://youtube.com/philosophers-stone',2);
INSERT INTO movie (movie_id, movie_title, movie_genre, movie_year, movie_director, movie_picture, movie_trailer,franchise_id)
VALUES (5, 'Iron Man', 'Action, Adventure', 2008, 'Jon Favreau', 'https://movies.com/Iron-Man', 'https://youtube.com/iron-man',3);

INSERT INTO character (id, char_name, char_alias, gender, char_picture)
VALUES (1, 'Iron Man', 'Tony Stark', 'MALE', 'https://example.com/ironman.jpg');
INSERT INTO character (id, char_name, char_alias, gender, char_picture)
VALUES (2, 'Black Widow', 'Natasha Romanoff', 'FEMALE', 'https://example.com/blackwidow.jpg');
INSERT INTO character (id, char_name, char_alias, gender, char_picture)
VALUES (3, 'Captain America', 'Steve Rogers', 'MALE', 'https://example.com/captainamerica.jpg');
package com.example.assignment3.Models;

import com.example.assignment3.Enum.Gender;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Setter
@Getter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="char_name",length = 50,nullable = false)
    private String name;
    @Column(name = "char_alias",length = 50)
    private String alias;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(name = "char_picture",length = 50,nullable = false)
    private String picture;

    @JsonIgnore
    @ManyToMany(mappedBy = "character")
    private Set<Movie> movie;


}

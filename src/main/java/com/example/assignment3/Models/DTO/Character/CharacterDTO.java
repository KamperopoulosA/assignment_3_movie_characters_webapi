package com.example.assignment3.Models.DTO.Character;

import com.example.assignment3.Enum.Gender;
import com.example.assignment3.Models.Movie;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CharacterDTO {
    private Long id;
    private String name;
    private String alias;
    private Gender gender;
    private String picture;
    private Set<Long> movie;
}

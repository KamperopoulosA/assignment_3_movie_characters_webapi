package com.example.assignment3.Models.DTO.Franchise;


import jakarta.persistence.Column;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
public class FranchiseDTO {
    private Long id;
    private String name;
    private String desc;
    private Set<Long> movie;
}

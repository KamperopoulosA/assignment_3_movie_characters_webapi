package com.example.assignment3.Models.DTO.Movie;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieListDTO {
    private Long id;
    private String title;
    private String genre;
    private Long year;
    private String director;
    private String picture;
    private String trailer;
}

package com.example.assignment3.Models.DTO.Movie;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private Long id;
    private String title;
    private String genre;
    private int year;
    private String director;
    private String picture;
    private String trailer;
    private int franchise;
    private Set<Long> character;


}

package com.example.assignment3.Models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.util.*;

@Entity
@Setter
@Getter
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="movie_id")
    private Long id;
    @Column(name = "movie_title",length = 50,nullable = false)
    private String title;
    @Column(name = "movie_genre",length = 50,nullable = false)
    private String genre;
    @Column(name = "movie_year")
    private Long year;
    @Column(name = "movie_director",length = 50,nullable = false)
    private String director;
    @Column(name="movie_picture",length = 100)
    private String picture;
    @Column(name = "movie_trailer",length = 100)
    private String trailer;

    @ManyToOne
    @JoinColumn(name="franchise_id")
    private Franchise franchise;

    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "character_id")
    )
    private Set<Character> character;


}

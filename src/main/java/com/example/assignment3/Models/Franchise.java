package com.example.assignment3.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Setter
@Getter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="fr_id")
    private Long id;
    @Column(name = "fr_name",length = 50,nullable = false)
    private String name;
    @Column(name = "fr_desc",length = 200,nullable = false)
    private String desc;

    @JsonIgnore
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movie;

}

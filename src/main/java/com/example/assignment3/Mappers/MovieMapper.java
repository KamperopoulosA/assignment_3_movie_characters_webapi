package com.example.assignment3.Mappers;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.DTO.Movie.MovieDTO;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.Character.CharacterService;
import com.example.assignment3.Services.Franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;



@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected FranchiseService franchiseService;

    /*
      Maps Movies to MovieDTO,
      param movie Movie to map,
      return mapped MovieDTO
     */


    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "character", source = "character", qualifiedByName = "characterToCharacterID")
    public abstract MovieDTO movieToMovieDTO(Movie movie);


    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movie);

    @Named(value = "characterToCharacterID")
    Set<Long> map(Set<Character> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(Long id) {
        return franchiseService.findById(id);
    }

    @Named("characterIdsToCharacter")
    Set<Character> mapIdsToCharacter(Set<Long> id) {
       return id.stream()
                .map( i-> characterService.findById(i))
                .collect(Collectors.toSet());
    }

      /*
      Maps MovieDTO to Movies.
      param movieDTO MovieDTO to map.
      return mapped Movies.
     */

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "character", source = "character", qualifiedByName = "characterIdsToCharacter")
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);
}


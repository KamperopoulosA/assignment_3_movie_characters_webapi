package com.example.assignment3.Mappers;


import com.example.assignment3.Models.DTO.Franchise.FranchiseDTO;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.Movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

      /*
         Maps Franchise to FranchiseDTO,
         param franchise Franchise to map,
         return mapped FranchiseDTO.
     */

    @Mapping(target = "movie", source = "movie", qualifiedByName = "movieToMovieID")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);


    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchise);
    
    
     /*
      Maps Movies to DTO,
      param movies,
      return movies.
     */

    
    @Named(value = "movieToMovieID")
    Set<Long> map(Set<Movie> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }
    /*
      Maps Movies from DTO,
      param ID,
      return id.
     */

    @Named("movieIdsToMovie")
    Set<Movie> mapIdsToMovie(Set<Long> id) {
        return id.stream()
                .map( i-> movieService.findById(i))
                .collect(Collectors.toSet());
    }
    @Mapping(target = "movie", source = "movie", qualifiedByName = "movieIdsToMovie")
    public abstract Franchise franchiseDTOToFranchise(FranchiseDTO franchise);

}

package com.example.assignment3.Mappers;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.DTO.Character.CharacterDTO;
import com.example.assignment3.Models.DTO.Movie.MovieDTO;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.Movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;

     /*
     * Maps Character to CharacterDTO.
     * param character Character to map.
     * return mapped Movies.
     */

    @Mapping(target = "movie", source = "movie", qualifiedByName = "movieToMovieID")
    public abstract CharacterDTO characterToCharacterDTO(Character character);
    public abstract Collection<CharacterDTO> characterToCharacterDTO(Collection<Character> character);

     /*
      Maps Movies to Ids,
      param movies,
      return movies.
     */

    @Named(value = "movieToMovieID")
    Set<Long> map(Set<Movie> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }

     /*
      Maps to MovieCharacters
      param movieIds,
      return movieIds.
     */

    @Named("movieIdsToMovie")
    Set<Movie> mapIdsToMovie(Set<Long> id) {
        return id.stream()
                .map( i-> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    @Mapping(target = "movie", source = "movie", qualifiedByName = "movieIdsToMovie")
    public abstract Character characterDTOToCharacter(CharacterDTO characterDTO);
}

package com.example.assignment3.Controllers;

import com.example.assignment3.Mappers.FranchiseMapper;
import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.DTO.Character.CharacterDTO;
import com.example.assignment3.Models.DTO.Franchise.FranchiseDTO;
import com.example.assignment3.Models.DTO.Movie.MovieDTO;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.Franchise.FranchiseService;
import com.example.assignment3.Services.Movie.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieService movieService;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieService movieService) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieService = movieService;
    }


/*
        Gets all franchises,
        return Ok response.
*/


    @GetMapping
    @Operation(summary = "Gets all franchises")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            )
    })
    public ResponseEntity findAll() {
        Collection<FranchiseDTO> franchise = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchise);
    }

    /*
        Gets a specific franchise by its ID,
        return Ok response.
    */

    @GetMapping("{id}")
    @Operation(summary = "Gets a franchise by its ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = FranchiseDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public ResponseEntity findById(@PathVariable Long id) {
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id));
        return ResponseEntity.ok(franchise);
    }

     /*
        Post operation
        adds a new  franchise to the database,
        return Created response.

    */

    @PostMapping
    @Operation(summary = "Adds a new Franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            )
    })
    public ResponseEntity<FranchiseDTO> add(@RequestBody FranchiseDTO franchiseDTO) throws URISyntaxException {
        Franchise franchise = franchiseMapper.franchiseDTOToFranchise(franchiseDTO);
        franchise = franchiseService.add(franchise);
        FranchiseDTO result = franchiseMapper.franchiseToFranchiseDTO(franchise);
        URI uri =  new URI("api/v1/franchises/" + result.getId());
        return ResponseEntity.created(uri).build();

    }

        /* 
        Update operation which updates franchise with specified ID,
        return ok response.

        */


    @PutMapping("{id}")
    @Operation(summary = "Updates a Franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ProblemDetail.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ProblemDetail.class)))
                    }
            )
    })
    public ResponseEntity<FranchiseDTO> update(@PathVariable Long id, @RequestBody FranchiseDTO franchiseDTO) {
        Franchise franchise = franchiseMapper.franchiseDTOToFranchise(franchiseDTO);
        Franchise updatedFranchise = franchiseService.update(id, franchise);
        FranchiseDTO updatedFranchiseDTO = franchiseMapper.franchiseToFranchiseDTO(updatedFranchise);
        return ResponseEntity.ok(updatedFranchiseDTO);
    }

     /*
        Delete operation which deletes franchise with specified ID,
        return ok response.
     */


    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes a franchise by its ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = FranchiseDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public void deleteById(@PathVariable Long id) {
        franchiseService.deleteById(id);
    }

      /* 
        Get all movies of a franchise,
        return ok response.

    */


    @GetMapping("{id}/movies")
    @Operation(summary = "Gets Franchise movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            )
    })
    public ResponseEntity getMovie(@PathVariable Long id) {
        return ResponseEntity.ok(franchiseService.getMovie(id));
    }

    /*    
          
          Update operation which updates franchise  movies,
          return no content response.
    */


    @PutMapping("{id}/movies")
    @Operation(summary = "Updates movies in a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ProblemDetail.class)))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ProblemDetail.class)))
                    }
            )
    })
    public ResponseEntity updateMovies(@PathVariable Long id, @RequestBody Long[] MovieIds) {
        franchiseService.updateMovie(id, MovieIds);
        return ResponseEntity.noContent().build();
    }




}

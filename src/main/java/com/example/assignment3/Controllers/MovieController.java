package com.example.assignment3.Controllers;

import com.example.assignment3.Mappers.MovieMapper;
import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.DTO.Character.CharacterDTO;
import com.example.assignment3.Models.DTO.Movie.MovieDTO;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.Movie.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;


@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;


    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;

    }

    /*
        Gets all movies,
        return Ok response.
    */


    @GetMapping
    @Operation(summary = "Gets all Movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    }
            )
    })

    public ResponseEntity findAll() {
        Collection<MovieDTO> movie = movieMapper.movieToMovieDTO(
                movieService.findAll()
        );
        return ResponseEntity.ok(movie);
    }

    /*
        Gets a specific movie by its ID,
        return Ok response.
    */

    @GetMapping("{id}")
    @Operation(summary = "Gets a movie by its ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = MovieDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })

    public ResponseEntity findById(@PathVariable Long id) {
        MovieDTO movie = movieMapper.movieToMovieDTO(movieService.findById(id));

        return ResponseEntity.ok(movie);
    }


     /*
        Post operation
        adds a new  movie to the database,
        return Created response.

    */

    @PostMapping
    @Operation(summary = "Adds a new movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })

    public ResponseEntity<MovieDTO> add(@RequestBody MovieDTO movieDTO) {
        Movie movie = movieMapper.movieDtoToMovie(movieDTO);
        Movie savedmovie = movieService.add(movie);
        MovieDTO result = movieMapper.movieToMovieDTO(savedmovie);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /* 
        Update operation which updates movie with specified ID,
        return no content response.

    */


    @PutMapping("{id}")
    @Operation(summary = "Updates a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })

    public ResponseEntity<MovieDTO> update(@PathVariable Long id, @RequestBody MovieDTO movieDTO) {
        Movie movie = movieMapper.movieDtoToMovie(movieDTO);
        Movie updatedMovie = movieService.update(id, movie);
        MovieDTO updatedMovieDTO = movieMapper.movieToMovieDTO(updatedMovie);
        return ResponseEntity.ok(updatedMovieDTO);
    }


        /*
         Delete operation which deletes franchise with specified ID,
                return ok response.
        */

    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes a movie by its ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = MovieDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public void deleteById(@PathVariable Long id) {
        movieService.deleteById(id);
    }

         /*
        
                
        Get all movies  franchise's,
        return ok response.

         */



    @GetMapping("{id}/franchises")
    @Operation(summary = "Gets Movie Franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = @Content
            )
    })
    public ResponseEntity getFranchise(@PathVariable Long id) {
        return ResponseEntity.ok(movieService.getFranchise(id));
    }

            /*
                Get all character  franchise's,
                return ok response.

            */

    @GetMapping("{id}/characters")
    @Operation(summary = "Gets Movie Characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = @Content
            )
    })
    public ResponseEntity getCharacter(@PathVariable Long id) {
        return ResponseEntity.ok(movieService.getCharacter(id));
    }

         /*
               Update a character in movie,
               return no content response.

            */

    @PutMapping("{id}/characters")
    @Operation(summary = "Updates characters in a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity updateCharacters(@PathVariable Long id, @RequestBody Long[] CharacterIds) {
        movieService.updateCharacter(id, CharacterIds);
        return ResponseEntity.noContent().build();
    }





    }











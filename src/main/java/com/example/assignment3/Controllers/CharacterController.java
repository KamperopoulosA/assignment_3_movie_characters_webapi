package com.example.assignment3.Controllers;

import com.example.assignment3.Mappers.CharacterMapper;
import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.DTO.Character.CharacterDTO;
import com.example.assignment3.Services.Character.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

  /*
         Gets all movie characters,
         return Ok response.
   */

    @GetMapping
    @Operation(summary = "Gets all Characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            )
    })

    public ResponseEntity findAll() {
        Collection<CharacterDTO> character = characterMapper.characterToCharacterDTO(
                characterService.findAll()
        );
        return ResponseEntity.ok(character);
    }

    /**
        Gets a specific character by its ID,
        return Ok response.
   */

    @GetMapping("{id}")
    @Operation(summary = "Gets a character by its ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public ResponseEntity findById(@PathVariable Long id) {
        CharacterDTO character = characterMapper.characterToCharacterDTO(characterService.findById(id));

        return ResponseEntity.ok(character);
    }


      /*
        Post operation,
        adds a new  character to the database,
        return Created response.
   */

    @PostMapping
    @Operation(summary = "Adds a new Character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            )
    })
    public ResponseEntity<CharacterDTO> add(@RequestBody CharacterDTO characterDTO) {
        Character character = characterMapper.characterDTOToCharacter(characterDTO);
        character = characterService.add(character);
        CharacterDTO result = characterMapper.characterToCharacterDTO(character);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /* 
        Update operation which updates character with specified ID,
        return no content response.

    */

    @PutMapping("{id}")
    @Operation(summary = "Updates a Character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class))
                    }
            )
    })
    public ResponseEntity<CharacterDTO> update(@PathVariable Long id, @RequestBody CharacterDTO characterDTO) {
        Character character = characterMapper.characterDTOToCharacter(characterDTO);
        Character updatedCharacter = characterService.update(id, character);
        CharacterDTO updatedCharacterDTO = characterMapper.characterToCharacterDTO(updatedCharacter);
        return ResponseEntity.ok(updatedCharacterDTO);
    }

    /*
        Delete operation which deletes character with specified ID,
        return ok response.
    */

    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a character by its ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public void deleteById(@PathVariable Long id){
        characterService.deleteById(id);
    }

    /* 
        Get all movies of a character,
        return ok response.

    */

    @GetMapping("{id}/movies")
    @Operation(summary = "Gets Character movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            )
    })
    public ResponseEntity getMovie(@PathVariable Long id) {
        return ResponseEntity.ok(characterService.getMovie(id));
    }
        /*
          
          
          Update operation which characters movies,
          return no content response.

        */



    @PutMapping("{id}/movies")
    @Operation(summary = "Updates movies in a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ProblemDetail.class))
                    }
            )
    })
    public ResponseEntity updateMovies(@PathVariable Long id, @RequestBody Long[] MovieIds) {
        characterService.updateMovie(id, MovieIds);
        return ResponseEntity.noContent().build();
    }


}

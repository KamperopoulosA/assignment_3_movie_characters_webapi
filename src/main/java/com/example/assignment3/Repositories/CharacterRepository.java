package com.example.assignment3.Repositories;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


/**
 * Repository for the Character domain class.
 */


public interface CharacterRepository extends JpaRepository<Character,Long> {
    <S extends Character> S save(S entity);
    Optional<Character> findById(Long primaryKey);
    List<Character> findAll();

    void deleteById(Long primaryKey);
    boolean existsById(Long primaryKey);
}

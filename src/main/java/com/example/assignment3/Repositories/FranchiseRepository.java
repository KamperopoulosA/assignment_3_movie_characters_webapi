package com.example.assignment3.Repositories;

import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


/**
 * Repository for the Franchise domain class.
 */

public interface FranchiseRepository extends JpaRepository<Franchise,Long> {
    <S extends Franchise> S save(S entity);
    Optional<Franchise> findById(Long primaryKey);
    List<Franchise> findAll();

    void deleteById(Long primaryKey);
    boolean existsById(Long primaryKey);

}

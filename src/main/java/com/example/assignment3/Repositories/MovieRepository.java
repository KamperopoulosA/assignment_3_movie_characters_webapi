package com.example.assignment3.Repositories;

import com.example.assignment3.Models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * Repository for the Movie domain class.
 */

public interface MovieRepository extends JpaRepository<Movie,Long> {

    <S extends Movie> S save(S entity);
    Optional<Movie> findById(Long primaryKey);
    List<Movie> findAll();

    void deleteById(Long primaryKey);
    boolean existsById(Long primaryKey);


}

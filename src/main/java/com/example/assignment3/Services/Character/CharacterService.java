package com.example.assignment3.Services.Character;
import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.CrudService;

import java.util.Set;

/*

     Service for the Character domain class,
     Providing basic CRUD functionality through CrudService.


*/
public interface CharacterService extends CrudService<Character,Long> {
    Set<Movie> getMovie(Long CharacterId);
    void updateMovie(Long CharacterId, Long[] movie);
}

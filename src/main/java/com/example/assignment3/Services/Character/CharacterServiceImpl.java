package com.example.assignment3.Services.Character;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Repositories.MovieRepository;
import com.example.assignment3.Models.Character;
import com.example.assignment3.Repositories.CharacterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;



/**
  Implementation of the Character service.
  Using the Character repository to interact with the data store.
 */

@Service
public class CharacterServiceImpl implements CharacterService {
    private final Logger logger = LoggerFactory.getLogger(CharacterServiceImpl.class);
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;


    public CharacterServiceImpl(CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Character findById(Long id) {
        return characterRepository.findById(id).get();
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Long id,Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
       characterRepository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return characterRepository.existsById(id);
    }
    @Override
    public Set<Movie> getMovie(Long id) {
        return characterRepository.findById(id).get().getMovie();
    }

    @Override
    public void updateMovie(Long id, Long[] movie) {
        Character character = characterRepository.findById(id).get();
        Set<Movie> movieList = new HashSet<>();

        for (Long id1: movie) {
            movieList.add(movieRepository.findById(id1).get());
        }
        character.setMovie(movieList);
        characterRepository.save(character);
    }
}

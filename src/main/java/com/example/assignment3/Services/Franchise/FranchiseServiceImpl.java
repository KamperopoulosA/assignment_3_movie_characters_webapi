package com.example.assignment3.Services.Franchise;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Repositories.FranchiseRepository;
import com.example.assignment3.Repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/*
  Implementation of the Franchise service.
  Using the Franchise repository to interact with the data store.
 */


@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final Logger logger = LoggerFactory.getLogger(FranchiseServiceImpl.class);
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Long id) {
        return franchiseRepository.findById(id).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Long id,Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        franchiseRepository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return franchiseRepository.existsById(id);
    }
    @Override
    public Set<Movie> getMovie(Long id) {
        return franchiseRepository.findById(id).get().getMovie();
    }

    @Override
    public void updateMovie(Long id, Long[] movie) {
        Franchise franchise = franchiseRepository.findById(id).get();
        Set<Movie> movieList = new HashSet<>();

        for (Long id1: movie) {
            movieList.add(movieRepository.findById(id1).get());
        }
        franchise.setMovie(movieList);
        franchiseRepository.save(franchise);
    }
}

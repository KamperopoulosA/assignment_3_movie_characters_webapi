package com.example.assignment3.Services.Franchise;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.CrudService;

import java.util.Set;

/*

     Service for the Franchise domain class,
     Providing basic CRUD functionality through CrudService and extends it.


*/


public interface FranchiseService extends CrudService<Franchise,Long> {
    Set<Movie> getMovie(Long FranchiseId);
    void updateMovie(Long FranchiseId, Long[] movie);

}

package com.example.assignment3.Services.Movie;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Services.CrudService;

import java.util.Set;


/*

     Service for the Franchise domain class,
     Providing basic CRUD functionality through CrudService and extends it.


*/


public interface MovieService extends CrudService<Movie,Long> {
    Franchise getFranchise(Long MovieId);
    Set<Character> getCharacter(Long MovieId);
    void updateCharacter(Long MovieId, Long[] character);



}

package com.example.assignment3.Services.Movie;

import com.example.assignment3.Models.Character;
import com.example.assignment3.Models.Franchise;
import com.example.assignment3.Models.Movie;
import com.example.assignment3.Repositories.CharacterRepository;
import com.example.assignment3.Repositories.FranchiseRepository;
import com.example.assignment3.Repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/*
  Implementation of the Movie service.
  Using the Movie repository to interact with the data store.
 */



@Service
public class MovieServiceImpl implements MovieService{
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;
    private final FranchiseRepository franchiseRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository, FranchiseRepository franchiseRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Movie findById(Long id) {
        return movieRepository.findById(id).get();
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Long id,Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {movieRepository.deleteById(id);    }

    @Override
    public boolean exists(Long id) {
        return movieRepository.existsById(id);
    }

    @Override
    public Franchise getFranchise(Long id) {
        return movieRepository.findById(id).get().getFranchise();
    }

    @Override
    public Set<Character> getCharacter(Long id) {
        return movieRepository.findById(id).get().getCharacter();
    }
    @Override
    public void updateCharacter(Long id, Long[] character) {
        Movie movie = movieRepository.findById(id).get();
        Set<Character> characterList = new HashSet<>();

        for (Long id1: character) {
            characterList.add(characterRepository.findById(id1).get());
        }
        movie.setCharacter(characterList);
        movieRepository.save(movie);
    }







}

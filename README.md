# Assignment_3_Movie_Characters_WebAPI

## Introduction and Overview
This application is designed to store and manipulate movie characters information. It is built using Spring Web and PostgreSQL through Hibernate, with a RESTful API for users to manipulate the data. The database will store information about characters, movies, and franchises that these movies belong to, and has the potential for expansion in the future. This application provides a comprehensive CRUD API for managing Movies, Characters, and Franchises. The API enables users to perform full CRUD operations on each entity, including adding, updating, reading, and deleting. The API also provides endpoints for updating related data, such as characters in a movie, or movies in a franchise. The API also provides reports, such as retrieving all the movies in a franchise or all the characters in a movie.

## Business Rules
The entities, characters, movies, and franchises, interact with each other according to the following rules:

One movie contains many characters, and a character can play in multiple movies.
One movie belongs to one franchise, but a franchise can contain many movies. 

## Data Requirements


**Character**
- Autoincremented Id
- Full name
- Alias (if applicable)
- Gender
- Picture (URL to photo - not storing the image)

**Movie**
- Autoincremented Id
- Movie title
- Genre (simple string of comma separated genres, no genre modeling required as a base)
- Release year
- Director (simple string name, no director modeling required as a base)
- Picture (URL to a movie poster)
- Trailer (YouTube link)

**Franchise**
- Autoincremented Id
- Name
- Description

##  API Requirements
The API follows a standard CRUD model, with full CRUD capabilities for each entity. When deleting resources, related data is not deleted. Instead, foreign keys are set to null. When adding new resources, related data is not added at the same time, and is instead deferred to an update.

In addition to generic update methods, the API provides dedicated endpoints for updating characters in a movie, and updating movies in a franchise. These endpoints take in an array of IDs in the body, along with the corresponding movie or franchise ID in the path.

To ensure proper naming and logic grouping, the API also provides reports for each entity, including all the movies in a franchise, all the characters in a movie, and all the characters in a franchise.

## Domain Representation
The domain and business logic are encapsulated in services and repositories. When retrieving or inserting data, related data is not returned as domain objects, but instead as IDs. This is achieved by creating DTOs and mapping between the domain entities and DTOs using MapStruct.

DTOs play an important role in decoupling the client from the domain, preventing over-posting, exposing internal schemas, and making it easier to transition between the domain and presentation.

## Documentation with Swagger
The API provides proper documentation using Swagger/OpenAPI to ensure easy understanding and use of the API by developers and end-users.


## Getting started


1. Clone the repository

```
git clone https://gitlab.com/KamperopoulosA/assignment_3_movie_characters_webapi.git
```

2. Build the project using Gradle

```
cd assignment_3_movie_characters_webapi
./gradlew build
```

3. Run the application
```
./gradlew bootRun
```

4. Access the API documentation using Swagger UI

   - Open your browser and navigate to http://localhost:8080/swagger-ui.html
   - Explore the API endpoints and test them using the Swagger UI interface.


## Contributing

- [Fotis Staikos](https://gitlab.com/f.staikos7)
- [Kamperopoulos Anastasios](https://gitlab.com/KamperopoulosA)

